import { useState, useEffect} from 'react';

// Bootstrap Components
import Form from 'react-bootstrap/Form';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

const Login = ()=>{

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(true);

	useEffect(()=>{
		let isEmailNotEmpty = email !== '';
		let isPasswordNotEmpty = password !== '';

		if (isEmailNotEmpty && isPasswordNotEmpty) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	},[email,password]);

	const login = (e)=>{
		e.preventDefault();
		alert('You are now logged in!')
	}

	return (
		<Container fluid>
			<h3>Login</h3>
			<Form onSubmit={login}>
				<Form.Group>
					<Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" required value={email} onChange={(e)=>setEmail(e.target.value)} />
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" required value={password} onChange={(e)=>setPassword(e.target.value)} />
				</Form.Group>
				<Button variant="success" type="submit" disabled={isDisabled}>Login</Button>
			</Form>
		</Container>
	)
}


export default Login;