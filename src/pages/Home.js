// Bootstrap Components
import Container from 'react-bootstrap/Container';

// App Components
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Course from '../components/Course';

const Home = ()=>{
	return (
		<Container fluid>
			<Banner />
			<Highlights />
			<Course />
		</Container>
	)
}

export default Home;